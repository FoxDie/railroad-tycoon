#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <fstream>
#include <math.h>
#include <string>

using namespace std;
//Подключение классов
#include "Station.h";
#include "Player.h";
//Иницилизация меню
int menu( Player player);
int main_menu();

bool Buy = false, start = true, Sale_in_step = false, direction = true;
//Сохранение данных
void Save_Data(Station** stations, Player player);

void Load_Data(Station** stations, Player* player);


int main() 
{	//Иницилизация объектов
		Station* stations[5] = {
		new Station("Station_1", 1),
		new Station("Station_2", 2),
		new Station("Station_3", 3),
		new Station("Station_4", 4),
		new Station("Station_5", 5)
		};
		Player player(stations);
	//Иницилизация объектов

	//Циклический вывод меню
	while(1)
	{
	switch(main_menu())
	{
	//Начать новую игру
	case 1:


		while (1)
		{
			switch (menu(player))
			{
			// Продажа вагона
			case 1:
				// Вывод расценок
				player.get_point()->show_products();

				//Продажа
				player.sale_railway();
				system("Pause");
				// Совершена продажа на этом ходу
				Sale_in_step = true;

				//Вывод вагонов игрока
				player.PrintRailways();
				break;
			//Покупка вагона
			case 2:
				// Проверка на доступность покупки
				if (Buy || start || Sale_in_step)
				{
					// Вывод расценок
					player.get_point()->show_products();

					//Покупка
					player.buy_railway();

					//Совершена первая покупка
					Buy = true;
					
				}
				else cout << "Firstly U must sold smth " << endl;
				//Вывод вагонов игрока
				player.PrintRailways();
				system("Pause");
				break;
			// Переход на следующую станцию
			case 3:
				//Если была совершена первая покупка
				if (Buy)
				{
					Buy = false;
					start = false;
				}
				// Покупок на этом шаге не совершалось
				Sale_in_step = false;

				//Если прибываем на конечную
				if (player.get_point()->getNum() == 5)
				{
					//Рерандомим цены, изменяем количество вагоно на станциях
					for (int i = 0; i < 5; i++)
					{
						stations[i]->rerandom_prices();
						stations[i]->add_product();
					}
					//Меняем направление движения
					direction = false;
				}
				//Если прибываем на начальную станцию
				else if (player.get_point()->getNum() == 0)
				{
					//Рерандомим цены, изменяем количество вагоно на станциях
					for (int i = 0; i < 5; i++)
					{
						stations[i]->rerandom_prices();
						stations[i]->add_product();
					}
					//Меняем направление движения
					direction = true;
				}

				//Если направление положтельное двигаемся вправо
				if (direction) player.next_station(stations);
				//Если направление отрицательное двигаемся влево
				else player.prev_station(stations);

				//Вывод вагонов игрока
				player.PrintRailways();
				break;
			//Вывод расценок на станции
			case 4:
				player.get_point()->show_products();
				system("Pause");
				break;
			//Сохранение игры
			case 5:

				//Сохранение вагонов
				player.FileRecord();

				//Сохранения игрока и станций
				Save_Data(stations, player);
				break;
			//Закончить игру
			case 6:
				//Вывод результата
				cout << player.get_deal_count() << endl;
				return 0;
			default:
				cout << "Incorect choice " << endl;
			}
		}
		break;
		
	//Загрузить игру
	case 2:
		// Загрузка данных о вагонах
		player.FileWrite();
		// Загрузка игрока и станций
		Load_Data(stations, &player);

		//Вывод меню
		while (1)
		{
			switch (menu(player))
			{
				// Продажа вагона
			case 1:
				// Вывод расценок
				player.get_point()->show_products();

				//Продажа
				player.sale_railway();

				// Совершена продажа на этом ходу
				Sale_in_step = true;

				//Вывод вагонов игрока
				player.PrintRailways();
				break;
				//Покупка вагона
			case 2:
				// Проверка на доступность покупки
				if (Buy || start || Sale_in_step)
				{
					// Вывод расценок
					player.get_point()->show_products();

					//Покупка
					player.buy_railway();

					//Совершена первая покупка
					Buy = true;

				}
				else cout << "Firstly U must sold smth " << endl;
				//Вывод вагонов игрока
				player.PrintRailways();
				system("Pause");
				break;
			// Переход на следующую станцию
			case 3:
				//Если была совершена первая покупка
				if (Buy)
				{
					Buy = false;
					start = false;
				}
				// Покупок на этом шаге не совершалось
				Sale_in_step = false;

				//Если прибываем на конечную
				if (player.get_point()->getNum() == 5)
				{
					//Рерандомим цены, изменяем количество вагоно на станциях
					for (int i = 0; i < 5; i++)
					{
						stations[i]->rerandom_prices();
						stations[i]->add_product();
					}
					//Меняем направление движения
					direction = false;
				}
				//Если прибываем на начальную станцию
				else if (player.get_point()->getNum() == 0)
				{
					//Рерандомим цены, изменяем количество вагоно на станциях
					for (int i = 0; i < 5; i++)
					{
						stations[i]->rerandom_prices();
						stations[i]->add_product();
					}
					//Меняем направление движения
					direction = true;
				}

				//Если направление положтельное двигаемся вправо
				if (direction) player.next_station(stations);
				//Если направление отрицательное двигаемся влево
				else player.prev_station(stations);

				//Вывод вагонов игрока
				player.PrintRailways();
				break;
				//Вывод расценок на станции
			case 4:
				player.get_point()->show_products();
				system("Pause");
				break;
				//Сохранение игры
			case 5:

				//Сохранение вагонов
				player.FileRecord();

				//Сохранения игрока и станций
				Save_Data(stations, player);
				break;
				//Закончить игру
			case 6:
				//Вывод результата
				cout <<"GAME OVER. Гёк result: " <<player.get_deal_count() << endl;
				return 0;
			default:
				cout << "Incorect choice " << endl;
			}
		}
		break;

	case 3:
		return 0;
	}


	}


	
	
	
}
//Главное меню
int main_menu()
{
	system("cls");
	cout << "\n";
	int ans;
	cout << "1. NEW GAME\n";
	cout << "2. LOAD GAME\n";
	cout << "3. EXIT\n";
	cin >> ans;
	return ans;
}

//Меню игры
int menu( Player player)
{
system("cls");
 cout << "\n";
 int ans;
 cout << "Choose\n";
 cout << "1-Sell\n";
 cout << "2-Buy\n";
 cout << "3-Next Station\n";
 cout << "4-Show prices\n";
 cout << "5-SAVE GAME\n";
 cout << "6-Exit\n";
 cout << "\n";

 cout << "Your money:" << player.getMoney()<<"\n";

 player.PrintRailways();


 cout << "U'r choice: ";
 cin >> ans;
 return ans;
}
