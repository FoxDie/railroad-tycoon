#ifndef PLAYER_H
#define PLAYER_H
#include <exception>

//Класс игрока
class Player
{
private:
	//Подключение двусвязного списка вагонов
	#include "Railways.h";
	//Деньги игрока
	int money;
	// Указатель на первій вагон
	Railways* head;
	//Указатель на текующую станцию
	Station* point;
	//Счет игрока
	int deal_count;
	
public:
	//Конструктор игрока
	Player(Station* stations[])
	{
		//Начальное значение денег
		money = 100;
		//Иницилизация вагонов
		InitList();
		// Начало движения с первой станции
		point = stations[0];
		// Счет равен нулю
		deal_count = 0;
	}

	//Иницилизация списка
	void InitList()
	{
		head = NULL;
	}
	//Иницилизация списка


	//Удаления вагона
	void DeletePost( Railways*& Pk)
	{
		if (Pk->next)
			Pk->next->prev = Pk->prev;
		if (Pk->prev)
			Pk->prev->next = Pk->next;
		else head = Pk->next;
		delete(Pk);

		Railways* temp = head;
		int i = 1;
		while (temp)
		{
			temp->number = i;
			i++;
			temp = temp->next;
		}

	}
	//Удаления вагона


	//Нахождения вагона по номеру
	Railways* FindItem( int number)
	{

		Railways* temp = head;

		while (temp != NULL)

			if (temp->number == number)
				return (temp);
			else temp = temp->next;

		return (NULL);
	}
	//Нахождения вагона по номеру


	//Добавление вагона в конец списка
	void AddInEnd( Product_type type)
	{
		if (!head)
		{
			head = new Railways;

			head->number = 1;
			head->type = type;
			head->next = NULL;
			head->prev = NULL;
		}
		else
		{
			int c = 2;
			Railways* Tail = head, * temp = new Railways;


			while (Tail->next)
			{
				Tail = Tail->next;
				c++;
			}

			temp->number = c;
			temp->type = type;

			temp->next = NULL;
			temp->prev = Tail;
			Tail->next = temp;
		}

	}
	//Добавление вагона в конец списка


	//Запись в файл списка
	void FileRecord()
	{
		Railways* temp = head;
		fstream file;
		file.open("Save_Railways.txt", ios::out);
		if (!temp) {
			cout << "Empty!\n";
			system("pause");
		}
		else do {
			file << temp->number<<"\t";
			file << temp->type << "\t";
			temp = temp->next;
		} while (temp);
		file.close();
		cout << "Success" << endl;
		system("pause");
	}
	//Запись в файл списка

	//Чтение из файла списка
	void FileWrite()
	{
		fstream file;
		file.open("Save_Railways.txt", ios::in);
		int check;
		if (head == nullptr) {
			Railways* temp = new Railways;
			file >> check;
			temp->number = check;
			file >> check;
			if (check == 0) temp->type = Product;
			else if (check == 1) temp->type = Tech;
			else if (check == 2) temp->type = Res;
			
			
			head = new Railways;
			head->number = temp->number;
			head->type = temp->type;
			
			head->next = NULL;
			head->prev = NULL;
		}
		for (;;) {
			check = -1;
			Railways* temp = new Railways;
			file >> check;
			if (check == -1) break;
			temp->number = check;
			file >> check;
			if (check == 0) temp->type = Product;
			else if (check == 1) temp->type = Tech;
			else if (check == 2) temp->type = Res;
			
			Railways* Tail = head;
			while (Tail->next)
				Tail = Tail->next;

			temp->next = NULL;

			temp->prev = Tail;

			Tail->next = temp;

		}
		file.close();
		cout << "Success" << endl;
		system("pause");
	}
	//Чтение из файла списка

	// Получение текущей станции игрока
	Station* get_point()
	{
		return point;
	};

	//Получения счета игрока
	int get_deal_count()
	{
		return deal_count;
	}

	//Узнать деньги игрока
	int getMoney(){return money;}

	//Установить значение для денег
	void setMoney(int a) { money = a; }

	// Вывод списка
	void PrintRailways()
	{
		Railways* temp = head;
		cout << "\nYour railways: " << endl;
		if (!temp)
			cout << "Empty \n\n";
		else
			do
			{
				cout << "| Number: " << temp->number << "  | Type: ";

				if (temp-> type == Product)
				{
					cout << "Product | " << endl;
				}
				else if (temp->type == Tech)
				{
					cout << "Tehnika | " << endl;
				}
				else 
				{
					cout << "Mineralu | " << endl;
				}
				cout << endl;
				temp = temp->next;
			} while (temp);
	}
	// Продаже вагона
	void sale_railway()
	{
		int number = 0;
		Railways* temp;
		cout << "Enter railway" << endl;
		cin >> number;
		temp = FindItem(number);
		if (temp == NULL) cout << "Railway not found";
		else if (temp->type = Product)
		{
			point->productu++;
			money += point->productu_sale;

			DeletePost(temp);
			deal_count++;
		}
		else if (temp->type == Tech) 
		{
			point->tehnika++;
			money += point->tehnika_sale; 

			DeletePost(temp);
			deal_count++;
		}
		else
		{
			point->mineralu++;
			money += point->mineralu_sale;

			DeletePost(temp);
			deal_count++;
		}

		

	};

	//Покупка вагона
	void buy_railway()
	{
		Railways* Tail = head;
		if ( head != nullptr)
		{
			while (Tail->next)
			{
				Tail = Tail->next;
			}
			if (Tail->number == 10)
			{
				cout << "U have MAX railways ";
			}
			else
			{
				int num = 0;
				cout << "What are U want to buy? " << endl << "1 - Product " << endl << "2 - Tehnika" << endl << "3 - Mineralu"<<endl;
				cin >> num;
				switch (num)
				{
				case 1:
					if (money > point->productu_buy && point->productu > 0)
					{
						point->productu--;
						money -= point->productu_buy;
						AddInEnd(Product);
					}
					else if (money < point->tehnika_buy) cout << "U dont have money for this.";
					else if (point->tehnika == 0) cout << "Tovar ne dostupen";
					if (money <= 0) 
					{
						cout << "GAME OVER. U'r result - " << get_deal_count() << endl;
						exit(0);
					}
					break;
				case 3:
					if (money > point->mineralu_buy&& point->mineralu > 0)
					{
						point->mineralu--;
						money -= point->mineralu_buy;
						AddInEnd(Res);
					}
					else if (money < point->tehnika_buy) cout << "U dont have money for this.";
					else if (point->tehnika == 0) cout << "Tovar ne dostupen";
					if (money <= 0)
					{
						cout << "GAME OVER. U'r result - " << get_deal_count() << endl;
						exit(0);
					}
					break;
				case 2:
					if (money > point->tehnika_buy&& point->tehnika > 0)
					{
						point->tehnika--;
						money -= point->tehnika_buy;
						AddInEnd(Tech);
					}
					else if (money < point->tehnika_buy) cout << "U dont have money for this.";
					else if (point->tehnika == 0) cout << "Tovar ne dostupen";
					if (money <= 0)
					{
						cout << "GAME OVER. U'r result - " << get_deal_count() << endl;
						exit(0);
					}
					break;
				}
			}
		}
		else
		{
			int num = 0;
			cout << "What are U want to buy? " << endl << "1 - Product " << endl << "2 - Tehnika" << endl << "3 - Mineralu" << endl;
			cin >> num;
			switch (num)
			{
			case 1:
				if (money > point->productu_buy&& point->productu > 0)
				{
					point->productu--;
					money -= point->productu_buy;
					AddInEnd(Product);
				}
				if (money <= 0)
				{
					cout << "GAME OVER. U'r result - " << get_deal_count() << endl;
					exit(0);
				}
				break;
			case 3:
				if (money > point->mineralu_buy&& point->mineralu > 0)
				{
					point->mineralu--;
					money -= point->mineralu_buy;
					AddInEnd(Res);
				}
				if (money <= 0)
				{
					cout << "GAME OVER. U'r result - " << get_deal_count() << endl;
					exit(0);
				}
				break;
			case 2:
				if (money > point->tehnika_buy&& point->tehnika > 0)
				{
					point->tehnika--;
					money -= point->tehnika_buy;
					AddInEnd(Tech);
				}
				else if (money < point->tehnika_buy) cout << "U dont have money for this.";
				else if (point->tehnika == 0) cout << "Tovar ne dostupen";
				if (money <= 0)
				{
					cout << "GAME OVER. U'r result - " << get_deal_count() << endl;
					exit(0);
				}
				break;
			}
		}
		
		
	};

	//Переход на следующую станцию
	void next_station(Station* stations[])
	{
		int save = point->number;
		if (save == 5) cout << "Konechnaya";
		else
		{
			save++;
			point = stations[save];
		}
		Railways* Tail = head;
		while (Tail->next)
			{
				Tail = Tail->next;
			}
		money -= 5 * Tail->number;
		if (money <= 0) 
		{
			cout << "GAME OVER. U'r result - " << get_deal_count() << endl;
			exit(0);
		}
	}

	//Переход на предыдущую станцию
	void prev_station(Station* stations[])
	{
		int save = point->number;
		if (save == 1) cout << "Konechnaya";
		else
		{
			save--;
			point = stations[save];
		}
		Railways* Tail = head;
		while (Tail->next)
			{
				Tail = Tail->next;
			}
		money -= 5 * Tail->number;
		if (money <= 0) 
		{
			cout << "GAME OVER. U'r result - " << get_deal_count() << endl;
			exit(0);
		}
	}
};

#endif
