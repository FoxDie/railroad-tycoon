#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <fstream>
#include <string>

using namespace std;
//Подключение классов
#include "Station.h";
#include "Player.h";


// Сохранения данных
void Save_Data(Station** stations, Player player)
{
	fstream file;
	file.open("Save_Data.txt", ios::out);
	file << player.getMoney() << endl;
	for (int i = 0; i < 5; i++)
	{
		file << stations[i]->getProd() << "\t";
		file << stations[i]->getProdB() << "\t";
		file << stations[i]->getProdS() << endl;

		file << stations[i]->getMin() << "\t";
		file << stations[i]->getMinB() << "\t";
		file << stations[i]->getMinS() << endl;

		file << stations[i]->getTec() << "\t";
		file << stations[i]->getTecB() << "\t";
		file << stations[i]->getTecS() << endl << endl;

	};
	file.close();
};


// Загрузка сохранения
void Load_Data(Station** stations, Player* player)
{
	fstream file;
	file.open("Save_Data.txt", ios::in);
	int money, set, setB, setS;
	file >> money;
	player->setMoney(money);
	for (int i = 0; i < 5; i++)
	{
		file >> set;
		stations[i]->setProd(set);
		file >> setB;
		stations[i]->setProdB(setB);
		file >> setS;
		stations[i]->setProdS(setS);

		file >> set;
		stations[i]->setMin(set);
		file >> setB;
		stations[i]->setMinB(setB);
		file >> setS;
		stations[i]->setMinS(setS);

		file >> set;
		stations[i]->setTec(set);
		file >> setB;
		stations[i]->setTecB(setB);
		file >> setS;
		stations[i]->setTecS(setS);

	};
	file.close();
}
