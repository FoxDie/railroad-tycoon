#ifndef STATION_H
#define STATION_H
class Player;
class Station
{
	//Называние станции
	string name;

	//Номер станции
	int number;

	//Количество продуктов
	int productu;
	// Цена на покупку продуктов
	int productu_buy;
	// Цена продажи продуктов
	int productu_sale;


	//Количество техники
	int tehnika;
	// Цена на покупку техники
	int tehnika_buy;
	// Цена продажи техники
	int tehnika_sale;


	//Количество минералов
	int mineralu;
	//Цена на покупку минералов
	int mineralu_buy;
	// Цена продажи минуралов
	int mineralu_sale;
	friend Player;
public:
	// Constructor by default
	Station()
	{
		this->name = "";
		productu = 0;
		tehnika = 0;
		mineralu = 0;
		number = 1;
	}

	//Конструктор станций
	Station(string name , int number)
	{
		this->name = name;
		this->number = number;

		productu = rand() % 5;
		productu_buy = (rand() % 60) + 20;
		productu_sale = (rand() % 60) + 20;

		tehnika = rand() % 5;
		tehnika_buy = (rand() % 60) + 20;
		tehnika_sale = (rand() % 60) + 20;

		mineralu = rand() % 5;
		mineralu_buy = (rand() % 60) + 20;
		mineralu_sale = (rand() % 60) + 20;
	}
	// Добавление количество товаров на станции
	void add_product()
	{
		this->productu += (rand() % 5) - 2;
		if (productu < 0) productu = 0;
		this->tehnika += (rand() % 5) - 2;
		if (tehnika < 0) tehnika = 0;
		this->mineralu += (rand() % 5) - 2;
		if (mineralu < 0) mineralu = 0;
	}

	//Рерандом цен на станции
	void rerandom_prices()
	{
		productu_buy = (rand() % 60) + 20;
		productu_sale = (rand() % 60) + 20;

		tehnika_buy = (rand() % 60) + 20;
		tehnika_sale = (rand() % 60) + 20;

		mineralu_buy = (rand() % 60) + 20;
		mineralu_sale = (rand() % 60) + 20;
	}

	//Вывод расценок
	void show_products()
	{
		cout << "\tProductu\t" << "Tehnika\t\t" << "Mineralu\t" << endl;
		cout << "Count\t" << productu << "\t\t" << tehnika << "\t\t" << mineralu << "\t" << endl;
		cout << "Buy\t" << productu_buy << "\t\t" << tehnika_buy << "\t\t" << mineralu_buy << "\t" << endl;
		cout << "Sale\t" << productu_sale << "\t\t" << tehnika_sale << "\t\t" << mineralu_sale << "\t" << endl<<endl;
	}
	//Геттеры
	int getProd(){return productu;}
	int getProdB(){return productu_buy;}
	int getProdS() { return productu_sale; }
	int getMin() { return mineralu; }
	int getMinB() { return mineralu_buy; }
	int getMinS() { return mineralu_sale; }
	int getTec() { return tehnika; }
	int getTecB() { return tehnika_buy; }
	int getTecS() { return tehnika_sale; }
	int getNum() { return number; }
	//Геттеры


	//Сеттеры
	void setProd(int a){productu=a;}
	void setProdB(int a){productu_buy=a;}
	void setProdS(int a) {productu_sale=a; }
	void setMin(int a) { mineralu=a; }
	void setMinB(int a) { mineralu_buy=a; }
	void setMinS(int a) { mineralu_sale=a; }
	void setTec(int a) {  tehnika=a; }
	void setTecB(int a) {  tehnika_buy=a; }
	void setTecS(int a) { tehnika_sale=a; }
	//Сеттеры
};

#endif
